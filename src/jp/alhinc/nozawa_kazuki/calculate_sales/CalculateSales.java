
package jp.alhinc.nozawa_kazuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		if (args.length != 1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		File filePath = new File(args[0], "branch.lst");
		BufferedReader br = null;
		BufferedWriter bw = null;

		Map<String, String> branch = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		List<File> salesFiles = new ArrayList<File>(8);


		if (filePath.exists() == false) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}
			try {
				File file = new File(args[0], "branch.lst");
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

//				System.out.println(file);
				String line;

				while ((line = br.readLine()) != null) {
					String[] eachStore = line.split(",");

					if (eachStore.length != 2 || eachStore[0].matches("^[0-9]{3}$") == false) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
					}

					branch.put(eachStore[0], eachStore[1]);
					branchSales.put(eachStore[0], 0L);
				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					if (br != null) {
						br.close();
					}
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
					return;
				}
			}

		File file = new File(args[0]);
		File files[] = file.listFiles();

		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
//			System.out.println(files);
			if (files[i].isFile() && fileName.matches("[0-9]{8}.+rcd$")) {
//				System.out.println(files);
				salesFiles.add(files[i]);
			}

		}
//		System.out.println(files);
		for (int i = 0; i < salesFiles.size()-1; i++) {
			String fileNames = salesFiles.get(i).getName();
			String digital = fileNames.substring(0,8);
			int number = Integer.parseInt(digital);

			fileNames = salesFiles.get(i + 1).getName();
			digital = fileNames.substring(0,8);
			int nextNumber = Integer.parseInt(digital);

			if (nextNumber - number != 1) {
				System.out.println("売上ファイル名が連番になっていません。");
				return;
			}
		}


		Long lastAmount;
		FileReader fr = null;

		try {
			for (int i = 0; i < salesFiles.size(); i++) {
				fr = new FileReader(salesFiles.get(i));
				br = new BufferedReader(fr);
				//			System.out.println(br);

				List<String> salesList = new ArrayList<String>(8);
				String line;

				while ((line = br.readLine()) != null) {
					salesList.add(line);
//										System.out.println(line);
				}

				fr.close();

				String errorFile = salesFiles.get(i).getName();

				if (salesList.size() != 2) {
					System.out.println(" <"+ errorFile +">"+"のフォーマットが不正です");
					return;
				}
				if (salesList.get(1).matches("^[0-9]+$") == false) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				if (branch.containsKey(salesList.get(0)) == false) {
					System.out.println(" <"+ errorFile +">"+"の支店コードが不正です");
					return;
				}

				lastAmount = branchSales.get(salesList.get(0));
				long salesAmount = Long.parseLong(salesList.get(1));
				lastAmount = lastAmount + salesAmount;

				if (10 <= String.valueOf(lastAmount).length()) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(salesList.get(0), lastAmount);

			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("closeできませんでした。");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
					return;
				}
			}
		}


		try {
			File newfile = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(newfile);
			bw = new BufferedWriter(fw);

			//			bw.write(branch.get(eachStore[0]);

			for (String key : branch.keySet()) {
//								System.out.println(key + "," + branch.get(key) + "," + branchSales.get(key));
				bw.write(key + "," + branch.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
					return;
				}
			}
		}
	}
}
